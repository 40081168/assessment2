﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Author : Liam J Morris
//Class : Module - Contains references and constraints on module variables
//Modified : 28/11/14

namespace cw2
{
    class Module
    {
        private string M_Name;
        private string Module_Code;
        private string Module_Leader;

        //get and set for the module name
        public string m_name
        {
            get { return M_Name; }
            set { M_Name = value; }
        }

        //get and set for module code
        public string module_code
        {
            get { return Module_Code; }
            set { Module_Code = value; }
        }

        //get and set for module leader
        public string module_leader
        {
            get { return Module_Leader; }
            set { Module_Leader = value; }
        }
    }
}
