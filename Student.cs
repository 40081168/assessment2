﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Author : Liam J Morris
//Class : Student - Contains references and constraints on student variables
//Modified : 11/11/14

namespace Assessment2
{
    class Student
    {

        //putting the S_ infront of the names identifies it to me that it is
        //for a call in particular like S_ for Student
        private string S_Name;
        private string S_Address;
        private string S_Email;
        private int Matric_No;

        //get and set for the student name
        public string s_name
        {
            get { return S_Name; }
            set { S_Name = value; }
        }

        //get and set for the student address
        public string s_address
        {
            get { return S_Address; }
            set { S_Address = value; }
        }

        //get and set for the student email
        public string s_email
        {
            get { return S_Email; }
            set { S_Email = value; }
        }

        //get and set for the students matric number
        public int matric_no
        {
            get { return Matric_No; }
            set { Matric_No = value; }
        }

    }
}
