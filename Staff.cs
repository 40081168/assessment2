﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Author : Liam J Morris
//Class : Staff - Contains references and constraints on staff variables
//Modified : 11/11/14

namespace Assessment2
{
    class Staff
    {
        private string L_Name;
        private string L_Address;
        private string L_Email;
        private int PayRoll_No;
        private string Department;
        private string Role;

        //get and set for the lecturer name
        public string l_name
        {
            get { return L_Name; }
            set { L_Name = value; }
        }

        //get and set for the lecturer address
        public string l_address
        {
            get { return L_Address; }
            set { L_Address = value; }
        }

        //get and set for the lecturer email
        public string l_email
        {
            get { return L_Email; }
            set { L_Email = value; }
        }

        //get and set for the lecturer payroll number
        public int payroll_no
        {
            get { return PayRoll_No; }
            set { PayRoll_No = value; }
        }

        //get and set for the lecturer department
        public string department
        {
            get { return Department; }
            set { Department = value; }
        }

        //get and set for the lecturer role
        public string role
        {
            get { return Role; }
            set { Role = value; }
        }
    }
}
