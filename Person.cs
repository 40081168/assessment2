﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Author : Liam J Morris
//Class : Person - Contains references and constraints on person, staff, student, module variables
//Modified : 05/12/14

namespace Assessment_2
{
    public class Person
    //Contains the references and constraints for the person class
    {
        public string nameVar;
        public string addressVar;
        public string eMail;

        public string Name
        //Get and set for the name variable used in every inherited class
        {
            get { return nameVar; }
            set { nameVar = value; }
        }

        public string Address
        //Get and set for the address used in every inherited class
        {
            get { return addressVar; }
            set { addressVar = value; }
        }

        public string Email
        //Get and set for the email used in every inherited class
        {
            get { return eMail; }
            set { eMail = value; }
        }
    }

    //Author : Liam J Morris
    //Class : Student - Contains references and constraints on student variables, inherited from person
    //Modified : 05/12/14

    public class Student : Person
    //Contains references and costraints for the student class
    {
        public int matricnoVar;
        
        public int MatricNo
        //Get and set for the matric number, which is only unique to the student
        {
            get { return matricnoVar; }
            set { matricnoVar = value; }
        }

        public string getDetails()
        //Reference to the person class inheriting the name and address variables
            //TEST : To see if the inheritence would work in the first place
        {
            return "Student: \n " + "Name : " + nameVar + " \n " + "Address : " + addressVar + " \n " + "E-Mail : " +
                   eMail + " \n " + "Matric Number : " + matricnoVar;
        }
    }

    //Author : Liam J Morris
    //Class : Staff - Contains references and constraints on staff variables, inherited from person
    //Modified : 05/12/14

    public class Staff : Person
    //Contains referneces and constraints for the staff class
        {
            public string roleVar;
            public string departmentVar;
            public int payrollno;

            public string Role
            //Get and set for the role, in this case is a list box presenting three options
            {
                get { return roleVar; }
                set { roleVar = value; }
            }

            public string Department
            {
                get { return departmentVar; }
                set { departmentVar = value; }
            }

            public int PayRollNo
            //Get and set for the pay roll number, which is unique to the staff
            {
                get { return payrollno; }
                set { payrollno = value; }
            }

            public string getStaffDetails()
            //Reference to the person class inheriting the name and address variables
                //TEST : To see if the inheritence would work in the first place
            {
                return "Staff: \n " + "Name : " + nameVar + " \n " + "Address : " + addressVar + " \n " + "E-Mail : " +
                   eMail + " \n " + "Role : " + roleVar + " \n " + "Department : " + departmentVar + " \n "
                   + "Pay Roll Number : " + payrollno;
            }
        }

    //Author : Liam J Morris
    //Class : Module - Contains references and constraints on module variables 
    //Modified : 08/12/14

    public class Module:Student
    {
            public string moduleName;
            public string moduleCode;
            public string moduleLeader;
            public string statusVar;
            public int markVar;

            public string ModuleName
            //Get and set for the module name
            {
                get { return moduleName; }
                set { moduleName = value; }
            }

            public string ModuleCode
            //Get and set for the module code, unique to the module
            {
                get { return moduleCode; }
                set { moduleCode = value; }
            }

            public string ModuleLeader
            //Get and set for module leader, will only be selected by staff name
            {
                get { return moduleLeader; }
                set { moduleLeader = value; }
            }

            public string Status
            {
                get { return statusVar; }
                set { statusVar = value; }
            }

            public int Mark
            {
                get { return markVar; }
                set { markVar = value; }
            }

            public string getModuleDetails()
            //Only bringing back the module information
            {
                return " Module: \n " + "Module Name : " + moduleName + " \n " + "Module Code : " + moduleCode + " \n " 
                        + "Module Leader : " + moduleLeader;
            }

            public string fullStudentDetails()
            //Only bringing back the full student details information
            {
                return " Student : \n " + "Name : " + nameVar + " \n " + "Address : " + addressVar + " \n " + "E-Mail : " +
                   eMail + " \n " + "Matric Number : " + matricnoVar + " \n " + "Module Name : " + moduleName + 
                   " \n " + "Module Code : " + moduleCode + " \n " + "Module Leader : " + moduleLeader;
            }

    }
}