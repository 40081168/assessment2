﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//Author : Liam J Morris
//Window : Studnet - Contains the buttons and methods for the student list window
//Modified : 05/12/14

namespace Assessment_2
{
    /// <summary>
    /// Interaction logic for CertificateWindow.xaml
    /// </summary>
    public partial class StudentWindow : Window
    {
        public StudentWindow()
        {
        }

        public StudentWindow(Person student)
        {
            InitializeComponent();

            //lblName.Content = (" Student Record 1 : " + Environment.NewLine + 
            //                   " Student Name : " + student.Name + Environment.NewLine +
            //                   " Student Matric Number : " + student.MatricNo + Environment.NewLine +
            //                   " Student Address : " + student.Address + Environment.NewLine +
            //                   " Student Email : " + student.Email);

            //lblCourse.Content = ("Has passed a course in " + student.Course);

            //Grade = student.YearMark.ToString();
            //if (student.YearMark < 50)
            //    student.Grade = "Fail";
            //if (student.YearMark >= 50 && student.YearMark < 60)
            //    student.Grade = "Capable";
            //else if (student.YearMark >= 60 && student.YearMark < 70)
            //    student.Grade = "Average";
            //else if (student.YearMark >= 70 && student.YearMark < 80)
            //    student.Grade = "Good";
            //else if (student.YearMark >= 80 && student.YearMark < 90)
            //    student.Grade = "Smart";
            //else if (student.YearMark >= 90 && student.YearMark <= 100)
            //    student.Grade = "Brainy";

            //lblGraded.Content = ("They were graded as " +  student.Grade);         
        }

         private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
