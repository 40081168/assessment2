﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//Author : Liam J Morris
//Window : Main - Contains the buttons, methods, dictionary and tests for validation for the main gui window
//Modified : 11/12/14

namespace Assessment_2
{
    /// <summary>
    /// Interaction for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            LoadModuleList();
            LoadStaffList();
            LoadStudentList();
        }

        public void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //To allow the use of tabs in the main window this void was created
        {
            //IDEA:TabControl Method to switch tab after user enters student details, takes you to module tab
        }
        
        public List<Person> students = new List<Person>();
        //Is the origin of the list of students, using a dictionary later to look up and edit details

        private void Btn_SaveDetails_Click(object sender, RoutedEventArgs e)
            // Takes the values contained in the text boxes and updates the Student class properties.
            {
                Student newStudent = new Student();
                //Creates a new entry into the student list
                newStudent.Name = Txt_StdntName.Text;
                newStudent.Email = Txt_StdntEmail.Text;
                newStudent.Address = Txt_StdnAddress.Text;
                newStudent.MatricNo = Convert.ToInt32(Txt_StdntMatricNo.Text);
                //Stores all the strings as the entries to the list
                students.Add(newStudent);
                //Adds them to the list

                //Validation Testing
                    //All validation done below is to test my ideas on ways to test string and int
                if (Txt_StdntName.Text.Length == 0 || Txt_StdntEmail.Text.Length == 0 || Txt_StdnAddress.Text.Length == 0 ||
                    Txt_StdntMatricNo.Text.Length == 0)
                    //TEST : Checks all the lengths of the string and if one of them is equal to 0 then return a error message
                {
                    MessageBox.Show("Check All Textboxes Have been Filled In");
                }
                if (newStudent.MatricNo < 1000 || newStudent.MatricNo > 9000)
                    //TEST : If the matric number is less than 1000 and greater than 9000 then return a error message
                {
                    MessageBox.Show("Invalid Matric No");
                }
                if (newStudent.Email.Contains("@")) { }
                    //TEST : If the string contains @ then true else returns a error message
                else
                {
                    MessageBox.Show("Invalid Email Address");
                }

            //Textbox clear after save
               Txt_StdntName.Clear();
               Txt_StdntEmail.Clear();
               Txt_StdnAddress.Clear();
               Txt_StdntMatricNo.Clear();
            }

        private void Btn_ListStudents_Click(object sender, RoutedEventArgs e)
        {
            //StudentWindow form = new StudentWindow();
            //form.lblList.Content = ListView;
            //form.Show();
            foreach (Student s in students)
            //////For each student in the student list create a message box showing there details, form method getDetails
            ////    //This needs to be changed to list of all students on one window
            {
                MessageBox.Show(s.getDetails());
            }
        }

        public List<Person> staff = new List<Person>();
        //Is the origin of the list of staff, using a dictionary later to look up and edit details

        private void Btn_SaveStaff_Click(object sender, RoutedEventArgs e)
        {
            Staff newStaff = new Staff();
            //Creates a new entry into the staff list
            newStaff.Name = Txt_StfName.Text;
            newStaff.Email = Txt_StfEmail.Text;
            newStaff.Address = Txt_StfAddress.Text;
            newStaff.PayRollNo = Convert.ToInt32(Txt_StfPayRollNo.Text);
            newStaff.Department = Txt_StfDepartment.Text;
            newStaff.Role = Role_ComboBox.Text;
            //Stores all the strings as the entries to the list
            staff.Add(newStaff);
            //Adds them to the list

              //Validation Testing
                    //All validation done below is to test my ideas on ways to test string and int
                if (Txt_StfName.Text.Length == 0 || Txt_StfEmail.Text.Length == 0 || Txt_StfAddress.Text.Length == 0 ||
                    Txt_StfPayRollNo.Text.Length == 0)
                    //TEST : Checks all the lengths of the string and if one of them is equal to 0 then return a error message
                {
                    MessageBox.Show("Check All Textboxes Have been Filled In");
                }
                if (newStaff.PayRollNo < 9000 || newStaff.PayRollNo > 9999)
                    //TEST : If the matric number is less than 1000 and greater than 9000 then return a error message
                {
                    MessageBox.Show("Invalid Pay Roll No");
                }
                if (newStaff.Email.Contains("@")) { }
                    //TEST : If the string contains @ then true else returns a error message
                else
                {
                    MessageBox.Show("Invalid Email Address");
                }

            //Text clear after save
               Txt_StfName.Clear();
               Txt_StfEmail.Clear();
               Txt_StfAddress.Clear();
               Txt_StfPayRollNo.Clear();
               Txt_StfDepartment.Clear();
               Role_ComboBox.Items.Clear();
            }

        private void Btn_ListStaff_Click(object sender, RoutedEventArgs e)
        {
            foreach (Staff s in staff)
            //For each staff in the staff list create a message box showing there details, form method getStaffDetails
                //This needs to be changed to list of all staff on one window
            {
                MessageBox.Show(s.getStaffDetails());
            }
        }

        private void Btn_ListStudentDetails_Click(object sender, RoutedEventArgs e)
        {
            foreach (Student s in students)
            ////////For each student in the student list create a message box showing there details, form method getDetails
            //////    //This needs to be changed to list of all students on one window
            {
                MessageBox.Show(s.getDetails());
            }
            //List<Person> parentValue = students;
            //StudentWindow form = new StudentWindow();
            //form.Show();
        }

        private List<Module> module = new List<Module>();
        //Is the origin of the list of module, using a dictionary later to look up and edit details

        private void Btn_ListModules_Click(object sender, RoutedEventArgs e)
        {
            foreach (Module m in module)
            {
                MessageBox.Show(m.getModuleDetails());
            }
        }

        private void LoadModuleList()
        {
            module.Add(new Module() { ModuleName = "Computer Graphics", ModuleCode = "SET08116", ModuleLeader = "Ben Kenwright" });
           
            module.Add(new Module() { ModuleName = "Software Engineering", ModuleCode = "SET08103", ModuleLeader = "Rob Kemmer" });

            module.Add(new Module() { ModuleName = "Web Technologies", ModuleCode = "SET08101", ModuleLeader = "Andrew Cumming" });

            module.Add(new Module() { ModuleName = "Software Development", ModuleCode = "SET08108", ModuleLeader = "Neil Urquhart" });

            module.Add(new Module() { ModuleName = "Database Systems", ModuleCode = "INF08104", ModuleLeader = "Brian Davidson" });

            module.Add(new Module() { ModuleName = "Systems And Services", ModuleCode = "CSN08101", ModuleLeader = "Alistair Armitage" });
        }

        private void LoadStudentList()
        {
            students.Add(new Student() { Name = "Rorie McLaughin", Address = "Gorgie Road", Email = "Rorie@Home.com", MatricNo = 1001 });
        }

        private void LoadStaffList()
        {
            staff.Add(new Staff() { Name = "Ben Kenwright", Address = "Napier House", Email = "B.Kenwright@napier.co.uk", PayRollNo = 9001, Department = "Computing", Role = "Lecturer" });
        }

        private void Btn_AddModule_Click(object sender, RoutedEventArgs e)
        {
            Module newModule = new Module();

            newModule.ModuleName = Txt_MName.Text;
            newModule.ModuleCode = Txt_MCode.Text;
            newModule.ModuleLeader = Txt_MLeader.Text;

            module.Add(newModule);

            if (Txt_Status.Text.Length == 0 || Txt_Mark.Text.Length == 0)
            {
                MessageBox.Show("New Module Added");
            }
            else
            {
                Txt_Status.Clear();
                Txt_Mark.Clear();
            }

            //Validation Testing
            //All validation done below is to test my ideas on ways to test string and int
            if (Txt_MName.Text.Length == 0 || Txt_MCode.Text.Length == 0 || Txt_MLeader.Text.Length == 0)
            //TEST : Checks all the lengths of the string and if one of them is equal to 0 then return a error message
            {
                MessageBox.Show("Check All Textboxes Have been Filled In");
            }
                Txt_MName.Clear();
                Txt_MCode.Clear();
                Txt_MLeader.Clear();
                Txt_Status.Clear();
                Txt_Mark.Clear();   
        }

        public void Btn_EnrollOnModule_Click(object sender, RoutedEventArgs e)
        {
            ////List<List<Module>> list_array = new List<List<Student>> { newModule, newStudent };
            //List<string> students = new List<string> {};
            //List<string> module = new List<string> {};
            
            //List<List<string>> list_array = new List<List<string>> { students, module };

            //MessageBox.Show(fullStudentDetails());
            //Module enrolledStudent = new Module();

            //enrolledStudent.Name = Txt_StdntName.Text;
            //enrolledStudent.Address = Txt_StdnAddress.Text;
            //enrolledStudent.Email = Txt_StdntEmail.Text;
            //enrolledStudent.MatricNo = Convert.ToInt32(Txt_StdntMatricNo.Text);
            //enrolledStudent.ModuleName = Txt_MName.Text;
            //enrolledStudent.ModuleCode = Txt_MCode.Text;
            //enrolledStudent.ModuleLeader = Txt_MLeader.Text;

            //module.Add(enrolledStudent);

            List<string> students = new List<string>();
            // Put whatever you want in the initial list
            List<string> module = new List<string>();
            // Put whatever you want in the second list
            students.AddRange(module);
            
        }

        private void Btn_EditDetails_Click(object sender, RoutedEventArgs e)
        {
            //foreach (enrolledStudent)
            //{
            //    MessageBox.Show(fullStudentDetails());
            //}
        }
    }
}






